<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/npm-%3E%3D5.5.0-blue.svg" />
  <img src="https://img.shields.io/badge/node-%3E%3D9.3.0-blue.svg" />
  <a href="https://github.com/kefranabg/readme-md-generator#readme" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://github.com/kefranabg/readme-md-generator/graphs/commit-activity" target="_blank">
    <img alt="Maintenance" src="https://img.shields.io/badge/Maintained%3F-yes-green.svg" />
  </a>
  <a href="https://github.com/kefranabg/readme-md-generator/blob/master/LICENSE" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/github/license/omerrarall/assigment-frontend" />
  </a>
</p>

### This project a simple to-do app. You can add your todos.
[Project](https://fe-assigment-nvz5q.ondigitalocean.app/)

---
## Todo Vue-GoLang App Assignment
- Frontend of this project was coded with Vue Js.
- Backend of this project was coded with [Golang](https://gitlab.com/MehmetYuceoz/be_assignment). 

Frontend side consist of 2 components.
- addTask
    - This component receives tasks from the user
- listTask
    - This component lists the tasks received from the user

---
### Install
```sh
npm install
```
### Usage
```sh
npm run dev
```

# Test
This project was made with a-tdd approach.
- A-TDD
    - It is a project laying approach in which test cases are developed to determine what the code will do without writing the code and to validate it when the code is written. This project was written with this approach

## Run tests
```sh
npm run test
```
## Run consumer test
```sh
npm run test:pact
```
