context('Actions', () => {
    it('successfully loads', () => {
        cy.visit('http://localhost:8080')
    })

    it('add new task if backend already run', () => {
        cy.visit('http://localhost:8080')
        cy.wait(300)
        cy.get('input.inputTask').type('task 1 key', { delay: 100 })
        cy.get('input.inputTask').type('{enter}')
        cy.wait(500)
        cy.get('input.inputTask').type('task 2 click', { delay: 100 })
        cy.get('.button').click()
        cy.wait(500)
        cy.get('.task').first().should('contain', 'task 1 key')
        cy.get('.task').last().should('contain', 'task 2 click')
        cy.wait(3000)
    })
})

