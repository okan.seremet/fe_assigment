
const assert = require('assert')
const { Pact, Matchers } = require('@pact-foundation/pact')
const { eachLike } = Matchers
import axios from 'axios';

describe('Pact with to-do API', () => {
  const provider = new Pact({
    port: 8888,
    consumer: 'Consumer',
    provider: 'Provider',
  })

  beforeAll(() => provider.setup())

  afterAll(() => provider.finalize())

  describe('when a call to the API is made', () => {
    beforeAll(async () => {
      return provider.addInteraction({
        state: 'there are tasks',
        uponReceiving: 'a request for tasks',
        withRequest: {
          path: '/listTodo',
          method: 'GET',
        },
        willRespondWith: {
          body: eachLike({
              id: 0,
              title: "Acceptance Task",
          }),
          status: 200,
          headers: {'Content-Type':'application/json charset=UTF-8', "Access-Control-Allow-Origin": "*",}
        },
      })
    })

    it('will receive the list of current tasks', async () => {
      await axios.get("http://localhost:8888/listTodo").then((response) => {
        assert.ok(response.data.length)
      })

    })
  })
})
