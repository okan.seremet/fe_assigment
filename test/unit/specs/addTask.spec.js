import { mount } from '@vue/test-utils'
import addTask from '@/components/addTask'

test('render title', () => {
    const wrapper = mount(addTask)
    const title = wrapper.find('.addTask .title')
    expect(title.text()).toBe("TO DO LIST")
})

test('render input area', () => {
    const wrapper = mount(addTask)
    const inputArea = wrapper.find('.addTask .inputTask')
    expect(inputArea.exists()).toBe(true)
})

test('render add button', () => {
    const wrapper = mount(addTask)
    const addButton = wrapper.find('.addTask')
    const addButton2 = addButton.find('.button')
    expect(addButton2.exists()).toBe(true)
})